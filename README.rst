=====
What?
=====

This project purpose is reformatting old technical ASCII-only documents, like NES 2C02, C64 VIC, etc. This is a **work in progress**, and some of the documents are only partially converted yet, so if you see missing images or strange blocks of unreadable text, it's likely that will be fixed in the future (or **you** can do it, `Feedback Wanted!`_)

I provide each original document (e.g. ``VIC/VIC-Article.txt``), the revamped copy in HTML format, although you can convert it to whatever format reStructuredText_ supports (e.g. ``VIC/VIC-Article/VIC-Article.html``) plus newly created images in PNG format (e.g. ``VIC/VIC-Article/Structure.png``), the sources for the main document (e.g. ``VIC/VIC-Article/VIC-Article.rst``) and for every generated image (e.g. ``VIC/VIC-Article/Structure.txt``).

====
Why?
====

This documents are the most useful ones when you're going to create an emulator (like a NES or Atari one) or you just want to understand how the system worked. However, plain text difficults reading in some places (like in tables, normal text is perfectly fine of course) and to navigate, and ASCII art is good only if you use a text editor with a monospaced font (and sometimes, you even need a specific font to read them, like Windows Terminal font). Also, beautiful formatting is pleasant to the eyes :).

====
How?
====

I'm using a few pieces of awesome technology:

reStructuredText_
	Used to add lots of formatting to the existing documents without almost any change. Simple to use and powerful.
ditaa_
	Creates awesome images from ASCII art, again without almost any change.

If you're going to edit any text, take into account I'm using UTF8 encoding for all of them.

================
Feedback Wanted!
================

If you have feedback to give, corrections or amends to add to any document, better CSS to style them... please open an issue and I'll take care of it! Or if you're the action type, fork this repository and open a pull request with your changes :)

.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _ditaa: http://ditaa.sourceforge.net/
